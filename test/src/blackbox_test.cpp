#include "blackbox_test.h"

void BlackboxRLCTest::setUp() {

}

void BlackboxRLCTest::tearDown() {

}

void BlackboxRLCTest::blackboxD1() {
    MenuPrincipal mp;
    mp.calculerCircuit(Circuit::TYPE_PASSE_BANDE, 765000, 2000);
    
	CPPUNIT_ASSERT_DOUBLES_EQUAL(1000.0, mp.getResistance(), 0.1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0795773, mp.getBobine(), 1/10e6);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(5.43909e-13, mp.getCondensateur(), 1/10e9);
}

void BlackboxRLCTest::blackboxD2() {
	MenuPrincipal mp;
    mp.calculerCircuit(Circuit::TYPE_STOPPE_BANDE, 765000, 2000);

	CPPUNIT_ASSERT_DOUBLES_EQUAL(1000.0, mp.getResistance(), 0.1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0795773, mp.getBobine(), 1/10e6);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(5.43909e-13, mp.getCondensateur(), 1/10e15);
}

void BlackboxRLCTest::blackboxD3() {
    MenuPrincipal mp;
    mp.calculerCircuit(Circuit::TYPE_PASSE_HAUT, 300000, 0);

	CPPUNIT_ASSERT_DOUBLES_EQUAL(1000.0, mp.getResistance(), 0.1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, mp.getBobine(), 1/10e3);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.81446e-13, mp.getCondensateur(), 1/10e15);
}

void BlackboxRLCTest::blackboxD4() {
    MenuPrincipal mp;
    mp.calculerCircuit(Circuit::TYPE_PASSE_HAUT, 300000, 0);

	CPPUNIT_ASSERT_DOUBLES_EQUAL(1000.0, mp.getResistance(), 0.1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, mp.getBobine(), 1/10e3);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.81446e-13, mp.getCondensateur(), 1/10e15);
}



