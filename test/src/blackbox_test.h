// Classe qui teste la classe Circuit
// Avec le framework CppUnit

// Librairies CppUnit nécessaires.
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Le fichier à tester, qui se trouve dans un répertoire différent.
#include "../../src/menuprincipal.h"

class BlackboxRLCTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(BlackboxRLCTest);
    	CPPUNIT_TEST(blackboxD1);
    	CPPUNIT_TEST(blackboxD2);
    	CPPUNIT_TEST(blackboxD3);
    	CPPUNIT_TEST(blackboxD4);
    	CPPUNIT_TEST_SUITE_END();
    
private:

public:
	// Fonctions d'échafaudage
    void setUp();
	void tearDown();
    
    // Tests en boîte noire
    void blackboxD1();
    void blackboxD2();
    void blackboxD3();
    void blackboxD4();
};




