#include "menuprincipal.h"

void MenuPrincipal::afficherMenuPrincipal() {

    std::cout << "Calcul des composantes d'un circuit RLC" << std::endl;
    std::cout << "=======================================" << std::endl;

    std::cout << "Quel circuit voulez-vous calculer ?" << std::endl;
    std::cout << "  1) Passe-bande" << std::endl;
    std::cout << "  2) Passe-haut" << std::endl;
    std::cout << "  3) Passe-bas" << std::endl;
    std::cout << "  4) Stoppe-bande" << std::endl;
    std::cout << std::endl;
    std::cout << "Choix : ";

    int choix = 0;
    std::cin >> choix;

    if ((choix < 1) || (choix > 4)) {
        std::cout << "Valeur invalide : arrêt" << std::endl;
        return;
    }

    int freq = this->demanderFrequence();
    int bp = 0;

    if (choix==Circuit::TYPE_PASSE_BANDE || choix==Circuit::TYPE_STOPPE_BANDE)
        bp = this->demanderBandePassante();

    this->calculerCircuit(choix, freq, bp);
}

int MenuPrincipal::demanderFrequence() {
    int freq = 0;
    std::cout << "Le filtre devrait être centré sur quelle fréquence (en Hertz) ? ";
    std::cin >> freq;

    return freq;
}

int MenuPrincipal::demanderBandePassante() {
    int bande = 0;
    std::cout << "Quelle devrait être la largeur de la bande passante (en Hertz) ? ";
    std::cin >> bande;

    return bande;
}

void MenuPrincipal::calculerCircuit(int choix, int freq, int bp) {
    Circuit le_circuit(choix);

    le_circuit.setFrequence(freq);
    le_circuit.setBandePassante(bp);

    le_circuit.calculerRLC();

    this->resistance = le_circuit.getOhms();
    this->bobine = le_circuit.getHenry();
    this->condensateur = le_circuit.getFarads();

    std::cout << std::endl;
    std::cout << "Résistance : " << this->resistance << " Ohms" << std::endl;
    std::cout << "Bobine : " << this->bobine << " Henry" << std::endl; 
    std::cout << "Condensateur : " << this->condensateur << " Farads" << std::endl;  
}


