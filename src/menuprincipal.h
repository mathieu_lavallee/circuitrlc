#ifndef __MENUPRINCIPAL__
#define __MENUPRINCIPAL__

#include <iostream>

#include "circuit.h"

class MenuPrincipal {
    private:
        double resistance;
        double bobine;
        double condensateur;
    public:
        void afficherMenuPrincipal();
        void calculerCircuit(int choix, int freq, int bp);
        int demanderFrequence();
        int demanderBandePassante();

        double getResistance() {return this->resistance;}
        double getBobine() {return this->bobine;}
        double getCondensateur() {return this->condensateur;}
};

#endif
