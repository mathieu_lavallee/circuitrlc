#include "circuit.h"

const int Circuit::TYPE_PASSE_BANDE = 1;
const int Circuit::TYPE_PASSE_HAUT = 2;
const int Circuit::TYPE_PASSE_BAS = 3;
const int Circuit::TYPE_STOPPE_BANDE = 4;

Circuit::Circuit(int le_type) {
    this->ohms = 0;
    this->henry = 0;
    this->farads = 0;
    this->type = le_type;
}

void Circuit::calculerRLC() {
    if (this->type == Circuit::TYPE_PASSE_BANDE) {
        this->calculerPasseBande();
    }
    else if (this->type == Circuit::TYPE_STOPPE_BANDE) {
        this->calculerStoppeBande();
    }
    else if (this->type == Circuit::TYPE_PASSE_HAUT) {
        this->calculerPasseHaut();
    }
    else if (this->type == Circuit::TYPE_PASSE_BAS) {
        this->calculerPasseBas();
    }
}

void Circuit::calculerValeurs() {
	// Transformer les Hz en rad/sec
	double freq_rad = this->frequence * 2 * 3.1416;
    double bp_rad = this->bande_passante * 2 * 3.1416;
	
    if (this->bande_passante == 0) {
        // Résistance fixée à 1kohms
	    this->ohms = 1000.0;
        // Bobine fixée à 1 henry
        this->henry = 1.0;
    }
    else {
        // Bande passante = R/L
        // Résistance fixée à 1kohms
        this->ohms = 1000.0;
        this->henry = (double)this->ohms / bp_rad;
    }
	
    // Fréquence = 1/racine(LC)
	this->farads = (double)1/(freq_rad*freq_rad*this->henry);
}

void Circuit::calculerPasseBande() {
	// Afficher le circuit
	std::cout << std::endl;
	std::cout << "o----OOOO----| |----" << std::endl;
	std::cout << "       L      C    /" << std::endl;
	std::cout << "in (low Z)         \\ Rout" << std::endl;
	std::cout << "                   /" << std::endl;
	std::cout << "o------------------" << std::endl;
	
	this->calculerValeurs();
}

void Circuit::calculerPasseHaut() {
	// Afficher le circuit
	std::cout << "o-----| |-------------" << std::endl;
	std::cout << "       C        O    /" << std::endl;
	std::cout << "in (low Z)      O L  \\ Rout" << std::endl;
	std::cout << "                O    /" << std::endl;
	std::cout << "o--------------------" << std::endl;

	this->calculerValeurs();
}

void Circuit::calculerPasseBas() {
	// Afficher le circuit
	std::cout << "o----OOOO-------------" << std::endl;
	std::cout << "       L        |    /" << std::endl;
	std::cout << "in (low Z)     ===C  \\ Rout" << std::endl;
	std::cout << "                |    /" << std::endl;
	std::cout << "o--------------------" << std::endl;

	this->calculerValeurs();
}

void Circuit::calculerStoppeBande() {
	// Afficher le circuit
	std::cout << "o----OOOO-----------" << std::endl;
	std::cout << "   |   L    |      |" << std::endl;
	std::cout << "   --| |-----      \\" << std::endl;
	std::cout << "      C            /" << std::endl;    
	std::cout << "in (low Z)         \\ Rout" << std::endl;
	std::cout << "                   |" << std::endl;
	std::cout << "o-------------------" << std::endl;

	this->calculerValeurs();
}





