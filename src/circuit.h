#ifndef __CIRCUIT__
#define __CIRCUIT__

#include <iostream>

class Circuit {
    private:
        double ohms;
        double henry;
        double farads;
        int frequence;
        int bande_passante;
        int type;

        void calculerPasseBande();
        void calculerPasseHaut();
        void calculerPasseBas();
        void calculerStoppeBande();

        void calculerValeurs();

	public:
        static const int TYPE_PASSE_BANDE;
        static const int TYPE_PASSE_HAUT;
        static const int TYPE_PASSE_BAS;
        static const int TYPE_STOPPE_BANDE;

        Circuit(int le_type);

        void calculerRLC();

        void setFrequence(int freq) {this->frequence = freq;}
        void setBandePassante(int bp) {this->bande_passante = bp;}

        int getFrequence() {return this->frequence;}
        int getBandePassante() {return this->bande_passante;}
        double getOhms() {return this->ohms;}
        double getHenry() {return this->henry;}
        double getFarads() {return this->farads;}
};

#endif
