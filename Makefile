# Exécutables
EXEC=exercice
EXECTEST=exercice_test
# Répertoires
SOURCE=src
BINAIRE=bin
TESTS=test

all:launch

# =======================
# Compilation du logiciel
# =======================

$(BINAIRE)/$(EXEC): $(BINAIRE)/main.o $(BINAIRE)/circuit.o $(BINAIRE)/menuprincipal.o
	g++ -o $@ $^

$(BINAIRE)/main.o: $(SOURCE)/main.cpp $(SOURCE)/menuprincipal.h
	mkdir -p $(BINAIRE)
	g++ -o $@ -c $<

$(BINAIRE)/menuprincipal.o: $(SOURCE)/menuprincipal.cpp $(SOURCE)/menuprincipal.h $(SOURCE)/circuit.h
	g++ -o $@ -c $<

$(BINAIRE)/circuit.o: $(SOURCE)/circuit.cpp $(SOURCE)/circuit.h
	g++ -o $@ -c $<

launch: $(BINAIRE)/$(EXEC)
	./$(BINAIRE)/$(EXEC)

# ===========
# Utilitaires
# ===========

clean:
	rm -rf $(BINAIRE)/*.o
	rm -rf $(BINAIRE)/$(EXEC)
	rm -rf $(TESTS)/$(BINAIRE)/*.o
	rm -rf $(TESTS)/$(BINAIRE)/$(EXECTEST)

# =================
# Tests du logiciel
# =================

test: $(TESTS)/$(BINAIRE)/$(EXECTEST)
	./$(TESTS)/$(BINAIRE)/$(EXECTEST)

$(TESTS)/$(BINAIRE)/$(EXECTEST): $(TESTS)/$(BINAIRE)/main.o $(TESTS)/$(BINAIRE)/blackbox_test.o $(BINAIRE)/menuprincipal.o $(BINAIRE)/circuit.o
	g++ -o $@ $^ -lcppunit

$(TESTS)/$(BINAIRE)/main.o: $(TESTS)/$(SOURCE)/main.cpp $(TESTS)/$(SOURCE)/blackbox_test.h
	mkdir -p $(TESTS)/$(BINAIRE)
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/blackbox_test.o: $(TESTS)/$(SOURCE)/blackbox_test.cpp $(TESTS)/$(SOURCE)/blackbox_test.h $(SOURCE)/circuit.h
	g++ -o $@ -c $<



